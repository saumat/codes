﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(web_programlama_odev.Startup))]
namespace web_programlama_odev
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
