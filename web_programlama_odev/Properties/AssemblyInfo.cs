﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Bir derlemeyle ilgili Genel Bilgiler şu yollarla denetlenir:
// denetlenir. Bir derlemeyle ilişkilendirilmiş bilgileri değiştirmek için bu
// öznitelik değerlerini değiştirin.
[assembly: AssemblyTitle("web_programlama_odev")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("web_programlama_odev")]
[assembly: AssemblyCopyright("Telif hakkı ©  2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// ComVisible özelliğinin false olarak ayarlanması bu derlemedeki türleri COM bileşenlerine
//COM görünmez kılar.  Bu derlemedeki bir türe şu kaynaktan erişmeniz gerekiyorsa:
// COM'dan erişmeniz gerekiyorsa, o türde ComVisible özniteliğini true olarak ayarlayın.
[assembly: ComVisible(false)]

// Bu proje COM'a açılmışsa aşağıdaki GUID typelib'in ID'si içindir
[assembly: Guid("a5090b9c-2b49-4230-8b59-d89019b8cd84")]

// Bir derlemenin sürüm bilgisi aşağıdaki dört değerden oluşur:
//
//      Ana Sürüm
//      İkincil Sürüm
//      Yapı Numarası
//      Düzeltme
//
// Tüm değerleri belirtebilir veya Gözden Geçirme ve Yapı Numaralarını varsayılan değerlerine döndürebilirsiniz
// gösterildiği gibi '*' ile varsayılan değer atayabilirsiniz:
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
